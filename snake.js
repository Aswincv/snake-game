var canvas=document.querySelector('canvas');
canvas.width = 440 ;
canvas.height = 440 ;
var context = canvas.getContext('2d');



document.addEventListener("keydown",direction);
function direction(event){
      if(event.keyCode == 37 && d != "RIGHT" ) {
           d = "LEFT";
      }else if (event.keyCode == 38 && d != "DOWN" ) {
           d = "UP";
      }else if (event.keyCode == 39 && d != "LEFT" ) {
           d = "RIGHT";
      }else if(event.keyCode == 40 && d != "UP" ) {
           d = "DOWN";
      }
}

var inter;
var pos = 20;
var snakePos = [];
var score = 0;
var dx = 20;
var dy = 0;
var d="RIGHT";
let snakeX;
let snakeY;
let flag=0;


/*snake inetial position*/
snakePos[0] = {
      posX : 220,
      posY : 220
    };
snakePos[1] = {
      posX : 200,
      posY : 220
    };
/*fruit inetial position*/
var fruitPos = {
  posX : Math.floor(Math.random() * 20 + 1)*pos,
  posY : Math.floor(Math.random() * 20 + 1)*pos
};


function checkCollision(){
    for( let i=0; i < snakePos.length ; i++){
          if( snakeX === snakePos[i].posX && snakeY === snakePos[i].posY){
             return true;
          } else {
          return false;
          }
     }
}

function gameOver(){

     if(snakeX<0 || snakeX>21*pos || snakeY < 0 || snakeY>21*pos || checkCollision()){
              clearInterval(inter);

       }

}



/*update the snake position*/
function updatePosition(){

       switch (d) {
         case "LEFT": snakeX-=pos;
                       break;
         case "UP": snakeY-=pos;
                       break;
         case "RIGHT": snakeX+=pos;
                       break;
         case "DOWN": snakeY+=pos;
                       break;
       }
      if ( flag != 1 ) {
          snakePos.pop();
      }else {
           flag = 0;
      }
      gameOver();
      snakePos.unshift( { posX : snakeX,
               posY : snakeY } );
}





/*draw the snake and fruit*/
function draw(){
 console.log('hai');
  /*clearing the screen*/
    context.clearRect(0,0,canvas.width,canvas.height);


  /*drawing fruit*/
     context.fillStyle = 'red';
     context.fillRect(fruitPos.posX, fruitPos.posY, pos, pos);

 /*drawing snake*/
     for(let x = 0; x < snakePos.length; x++ ){
         context.fillStyle = x==0?'black':'blue';
         context.fillRect(snakePos[x].posX, snakePos[x].posY, pos, pos);
      }


      snakeX = snakePos[0].posX;
      snakeY = snakePos[0].posY;


      /*food eaten*/
      if(snakeX==fruitPos.posX && snakeY==fruitPos.posY){
           score++;
           flag=1;
           fruitPos.posX = Math.floor(Math.random() * 20 + 1)*pos;
           fruitPos.posY = Math.floor(Math.random() * 20 + 1)*pos;
      }
    //  gameOver();
      updatePosition();


}
inter = setInterval(draw, 100);
